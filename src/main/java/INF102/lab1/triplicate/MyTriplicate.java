package INF102.lab1.triplicate;

import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        
        for (int i = 0; i < list.size(); i++) {
            int x = i - 1;
            int count = 0;
            
            while (x >= 0 && count != 2) {
                if (list.get(i).equals(list.get(x))) {
                    count++;
                }
                x--;
            }

            if (count == 2) {
                return list.get(i);
            }
        }
        return null;
    }
}
